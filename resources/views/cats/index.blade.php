@extends('layout')

@section('content')


<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

  <!-- Sidebar Toggle (Topbar) -->
  <form class="form-inline">
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
      <i class="fa fa-bars"></i>
    </button>
  </form>

</nav>
<!-- End of Topbar -->

<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="row">
    <div class="col-md-10">
      <h1 class="h3 mb-2 text-gray-800">Cats management</h1>
    </div>
    <div class="col-md-2">
      <div class="pull-right">
        <a class="btn btn-success" href="{{ route('cats.create') }}"> Create New Cat</a>
      </div>
    </div>
  </div>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      @if(session()->get('success'))
        <div class="alert alert-success">
          {{ session()->get('success') }}  
        </div>
      @endif
    </div>
    <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>ID</th>
                <th>Cat Name</th>
                <th>Age</th>
                <th>Breed</th>
                <th>Owner's name</th>
                <th>Description</th>
                <th colspan="2">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($cats as $cat)
                <tr>
                  <td>{{$cat->id}}</td>
                  <td>{{$cat->name}}</td>
                  <td>{{$cat->age}} years</td>
                  <td>{{$cat->breed}}</td>
                  <td>{{$cat->owner_name}}</td>
                  <td>{{$cat->description}}</td>
                  <td><a href="{{ route('cats.edit', $cat->id)}}" class="btn btn-primary">Edit</a></td>
                  <td>
                    <form action="{{ route('cats.destroy', $cat->id)}}" method="post">
                      @csrf
                      @method('DELETE')
                      <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
    </div>
  </div>

</div>




@endsection