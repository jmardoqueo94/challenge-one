@extends('layout')

<script src='https://api.mapbox.com/mapbox-gl-js/v2.6.0/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v2.6.0/mapbox-gl.css' rel='stylesheet' />

<style>
   body {
      margin: 0;
      padding: 0;
   }

   #map {
      top: 0;
      bottom: 0;
      width: 100%;
      height: 500px;
   }
   
   .marker {
      background-image: url('../assets/img/marker2.png');
      background-size: cover;
      width: 50px;
      height: 50px;
      border-radius: 50%;
      cursor: pointer;
   }

   .mapboxgl-popup {
      max-width: 200px;
   }

   .mapboxgl-popup-content {
      text-align: center;
      font-family: 'Open Sans', sans-serif;
   }
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
@section('content')

<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

  <!-- Sidebar Toggle (Topbar) -->
  <form class="form-inline">
      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
         <i class="fa fa-bars"></i>
      </button>
   </form>

</nav>
<!-- End of Topbar -->

<!-- Begin Page Content -->
<div class="container-fluid">

   <div class="row">
      <h1 class="h3 mb-2 text-gray-800">Cats Locations</h1>
   </div>

   <div class="card-body">
      <div class="row">
         <div class="col-md-12">
            <div id="map"></div>
         </div>
     </div>
   </div>
</div>


@endsection

<script>
   mapboxgl.accessToken = 'pk.eyJ1IjoiamNvcmVhczk0IiwiYSI6ImNsMGoycHVqOTA4aXQzanI1czNta3B0eGIifQ.s7QMkNotKRhHwZ7SjfecDw';

   $( document ).ready(function() {
      $.ajax({
         url:"{{ route('get_location_cat') }}",
         //type:"GET",
         success:function(response){
            if(response.success){
               console.log(response.data);
               geojson = response.data;

               var map = new mapboxgl.Map({
                  container: 'map',
                  style: 'mapbox://styles/mapbox/light-v10',
                  center: [-89.1090687,13.7154416],
                  zoom: 14
               });

               $.each(geojson, function (key, val) {

                  val.features.forEach(function (marker) {
                     // create a HTML element for each feature
                     var el = document.createElement('div');
                     el.className = 'marker';

                     // make a marker for each feature and add it to the map
                     new mapboxgl.Marker(el)
                     .setLngLat(marker.geometry.coordinates)
                     .setPopup(
                        new mapboxgl.Popup({ offset: 10 }) // add popups
                        .setHTML(
                           '<h4>Name </h4><span>' +
                           marker.properties.title +
                           '</span>'
                        )
                     )
                     .addTo(map);
                  });
               });
            }
         }
      });
   });

</script>