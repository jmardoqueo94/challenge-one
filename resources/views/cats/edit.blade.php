@extends('layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
	<div class="card-header">
		<h2>Edit Cat Data</h2>
	</div>
	<div class="card-body">
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div><br />
		@endif
		<form method="post" action="{{ route('cats.update', $cat->id ) }}">
			<div class="form-group">
				@csrf
				@method('PATCH')
				<label for="country_name">Cat Name:</label>
				<input type="text" class="form-control" name="name" value="{{ $cat->name }}"/>
			</div>
			<br>
			<div class="form-group">
				<label for="cases">Cat age :</label>
				<input type="text" class="form-control" name="age" value="{{ $cat->age }}"/>
			</div>
			<br>
			<div class="form-group">
				<label for="cases">Breed :</label>
				<input type="text" class="form-control" name="breed" value="{{ $cat->breed }}"/>
			</div>
			<br>
			<div class="form-group">
				<label for="cases">Owner's name :</label>
				<input type="text" class="form-control" name="owner_name" value="{{ $cat->owner_name }}"/>
			</div>
			<br>
			<div class="form-group">
				<label for="cases">Description :</label>
				<input type="text" class="form-control" name="description" value="{{ $cat->description }}"/>
			</div>
			<br>
			<div class="form-group">
				<label for="cases">Latitude Coords :</label>
				<input type="text" class="form-control" name="coords_lat" value="{{ $cat->coords_lat }}"/>
			</div>
			<br>
			<div class="form-group">
				<label for="cases">Longitude Coords :</label>
				<input type="text" class="form-control" name="coords_lng" value="{{ $cat->coords_lng }}"/>
			</div>
			<br>
			<button type="submit" class="btn btn-primary">Update Data</button>
		</form>
	</div>
</div>
@endsection