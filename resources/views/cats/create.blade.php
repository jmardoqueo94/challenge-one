@extends('layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
	<div class="card-header">
		<h2>Add Cat Data</h2>
	</div>
	<div class="card-body">
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div><br />
		@endif
			<form method="post" action="{{ route('cats.store') }}">
				<div class="form-group">
					@csrf
					<label for="cat_name">Cat Name:</label>
					<input type="text" class="form-control" name="name"/>
				</div>
				<br>
				<div class="form-group">
					<label for="cases">Age :</label>
					<input type="text" class="form-control" placeholder="In years" name="age"/>
				</div>
				<br>
				<div class="form-group">
					<label for="cases">Breed :</label>
					<input type="text" class="form-control" name="breed"/>
				</div>
				<br>
				<div class="form-group">
					<label for="cases">Owner's name :</label>
					<input type="text" class="form-control" name="owner_name"/>
				</div>
				<br>
				<div class="form-group">
					<label for="cases">Description :</label>
					<input type="text" class="form-control" name="description"/>
				</div>
				<br>
				<div class="form-group">
					<label for="cases">Latitude Coords :</label>
					<input type="text" class="form-control" name="coords_lat"/>
				</div>
				<br>
				<div class="form-group">
					<label for="cases">Longitude Coords :</label>
					<input type="text" class="form-control" name="coords_lng"/>
				</div>
				<br>
				<button type="submit" class="btn btn-primary">Add Cat</button>
			</form>
	</div>
</div>
@endsection