<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cats')->insert([
            'name' => 'Oliver',
			'age' => '2',
			'breed' => 'Siberiano',
			'owner_name' => 'Juan Perez',
			'description' => 'Is a small cat',
			'coords_lat' => '13.719878',
			'coords_lng' => '-89.094299',
            "created_at" =>  \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),
        ]);

        DB::table('cats')->insert([
            'name' => 'Rocko',
			'age' => '2',
			'breed' => 'Persa',
			'owner_name' => 'Camila Perez',
			'description' => 'Is a very pretty white cat',
			'coords_lat' => '13.720486',
			'coords_lng' => '-89.106083',
            "created_at" =>  \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),
        ]);
    }
}
