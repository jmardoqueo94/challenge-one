<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cats', function (Blueprint $table) {
			$table->id();
			$table->string('name');
			$table->string('age');
			$table->string('breed');
			$table->string('owner_name');
			$table->string('description');
			$table->string('coords_lat')->nullable();
			$table->string('coords_lng')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('cats');
	}
}
