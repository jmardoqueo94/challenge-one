<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CatsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('cats', CatsController::class);


Route::get('location_view', function () {
    return view('cats.location'); 
})->name('location_view');

Route::get('get_location_cat', 'App\Http\Controllers\CatsController@getCoords')->name('get_location_cat');