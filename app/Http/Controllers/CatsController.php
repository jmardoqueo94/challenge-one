<?php

namespace App\Http\Controllers;

use App\Models\Cat;
use Illuminate\Http\Request;

class CatsController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$cats = Cat::all();
	
		return view('cats.index',compact('cats'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('cats.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$request->validate([
			'name' => 'required',
			'age' => 'required|numeric',
			'breed' => 'required',
			'owner_name' => 'required',
			'description' => 'required',
			'coords_lat' => 'required',
			'coords_lng' => 'required',
		]);
	
		$cat = Cat::create($request->all());
	
		return redirect()->route('cats.index')
			->with('success','Cat created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\Cat  $cat
	 * @return \Illuminate\Http\Response
	 */
	public function show(Cat $cat)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\Cat  $cat
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Cat $cat)
	{
		//$cat = Cat::findOrFail($id);

		return view('cats.edit', compact('cat'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Cat  $cat
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$validatedData = $request->validate([
			'name' => 'required',
			'age' => 'required|numeric',
			'breed' => 'required',
			'owner_name' => 'required',
			'description' => 'required',
			'coords_lat' => 'required',
			'coords_lng' => 'required',
		]);

		$update = Cat::whereId($id)->update($validatedData);

		return redirect('/cats')->with('success', 'Cat Data is successfully updated');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Cat  $cat
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Cat $cat)
	{
		//$cat = Cat::findOrFail($id);
		$cat->delete();

		return redirect('/cats')->with('success', 'Cat Data is successfully deleted');
	}

	public function getCoords(){
		$cats = Cat::all();

		foreach ($cats as $key => $value) {
			$new_array[] = array(
				"type" => "FeatureCollection",
				"features" => array(
					array(
						"type" => "Feature",
						"geometry" => array(
							"type" => "Point",
							"coordinates" => array(
								$value->coords_lng, $value->coords_lat
							)
						),
						"properties" => array(
							"title" => $value->name,
						)
					)
				)
			);
		}

		$new_array = json_decode(json_encode($new_array), true);

		if(!empty($new_array)){
			return response()->json([
					'success' => true,
					'data' => $new_array
			], 200);
		}else{
			return response()->json([
					'error' => true,
					'data' => []
			], 400);
		}
	}
}
