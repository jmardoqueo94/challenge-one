<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 
        'price', 
        'name',
        'age',
        'breed',
        'owner_name',
        'description',
        'coords_lat',
        'coords_lng',
    ];
}
